<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>+Encomendas</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/mycss.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> <!-- link da biblioteca do w3 - -->

  </head>
  <body class="content">

    
    <?php
		
		require_once 'view/template/nav.php';

        if(!isset($_SESSION))
    { 
        session_start(); 
    } 
        if(!isset($_SESSION['id'])){
            require_once 'login.php';
        }
		else if (isset($_GET['formulario'])) {

        require_once 'model/ProdutoDAO.php';
        require_once 'model/ClienteDAO.php';
        require_once 'model/EncomendaDAO.php';

        $clientes = ClienteDAO::getAll();
        $produtos = ProdutoDAO::getAll();
        $encomendas = EncomendaDAO::getByIdUsuario();

			if($_GET['formulario'] == "cadastroc") {
				require_once 'view/cadastroC.php';
			}
			else if($_GET['formulario'] == "cadastrop") {
				require_once 'view/cadastroP.php';
			}
			else if($_GET['formulario'] == "cadastroe") {
				
		        require_once "view/cadastroE.php";
			}
            else if($_GET['formulario'] == "listar"){
                require_once "view/listarE.php";
            }
<<<<<<< HEAD
=======
            else if($_GET['formulario'] == "listarC"){
                require_once "view/listarCli.php";
            }
>>>>>>> a190205a7b25c866b952724c982371ca2ee81ea0
            else if($_GET['formulario'] == "detalhar"){
                $id = $_GET['idencomenda'];
                $encomenda = EncomendaDAO::getById($id);
                require_once 'view/detalharE.php';
            }
<<<<<<< HEAD
            else if($_GET['formulario'] == "listarC"){
                require_once "view/listarCli.php";
            }
=======
>>>>>>> a190205a7b25c866b952724c982371ca2ee81ea0
            else if($_GET['formulario'] == "apagar"){
                $id = $_GET['idencomenda'];
                EncomendaDAO::delete($id);

                header('Location: index.php?formulario=listar');                
            }
            else if($_GET['formulario'] == "mudarStatus"){
                $id = $_GET['idencomenda'];
                $novo_status = $_GET['status'];
                EncomendaDAO::updateStatus($id, $novo_status);

                header('Location: index.php?formulario=listar');                
            }
            else if($_GET['formulario'] == "editar"){
                $id = $_GET['idencomenda'];
                $encomenda = EncomendaDAO::getById($id);

                require_once 'view/editarE.php';

            }else if ($_GET['formulario'] == "main") {

                require_once 'view/template/main.php';
                //echo $_SESSION['id'];

            }
            else if ($_GET['formulario'] == "logout") {
                unset($_SESSION['id']);
                require_once 'login.php';
            }
		}else{
            
			require_once 'view/template/main.php';

		}

	?>

    <script type="text/javascript"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
   <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>


    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
        <script>
            $(document).ready(function(){
                $("#form_login").on("submit",function(e){

                    e.preventDefault();
                    $.ajax({ 
                        url: "validar.php",
                        type: 'POST',
                        data: $("#form_login").serialize(),
                        success: function(data) {
                            if(data=="1"){
                                window.location = "index.php?formulario=main";
                            }else {
                                alert("Dados errados!");
                            }
                            
                        },
                        fail:function(data){
                            alert("Burro, está errado.");
                        }
                    });
                });

            });
        </script>

  </body>
</html>
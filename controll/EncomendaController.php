<?php

require_once 'model/Conexao.php';
require_once 'model/EncomendaDAO.php';

class EncomendaController {
    
    public static function inserir(){

        
        $id_cliente = $_POST['id_cliente'];
        $id_produto = $_POST['id_produto'];
        $data_entrega = $_POST['data_entrega'];
        $horario_entrega = $_POST['horario_entrega'];
        $valor_encomenda = $_POST['valor_encomenda'];
        $valor_encomenda = str_replace(",",".", $valor_encomenda);
        $obs = $_POST['observacao'];
        if(!isset($_SESSION))
        { 
            session_start(); 
        }
        $idusuario = $_SESSION['id']; 

        $e = new Encomenda($id_produto, $id_cliente, $data_entrega, $horario_entrega, $valor_encomenda, $obs, $idusuario);
        
        EncomendaDAO::insert($e);

        require_once "index.php";
    }

    public static function atualizar(){

        $idE = $_POST['idE'];
        $id_cliente = $_POST['id_cliente'];
        $id_produto = $_POST['id_produto'];
        $data_entrega = $_POST['data_entrega'];
        $horario_entrega = $_POST['horario_entrega'];
        $valor_encomenda = $_POST['valor_encomenda'];
        $valor_encomenda = str_replace(",",".", $valor_encomenda);
        $obs = $_POST['observacao'];
        if(!isset($_SESSION))
        { 
            session_start(); 
        }
        $idusuario = $_SESSION['id'];

        $e = new Encomenda($id_produto, $id_cliente, $data_entrega, $horario_entrega, $valor_encomenda, $obs, $idusuario);
        
        EncomendaDAO::update($e, $idE);

        require_once "index.php";
    }

    public static function listar(){

      require_once 'model/EncomendaDAO.php';

      $encomendas = EncomendaDAO::getAll(); //listar por getbyidusuario

      require_once "view/cadastroE.php";

    }

    public static function cadastrar(){

      require_once 'model/ClienteDAO.php';
      require_once 'model/ProdutoDAO.php';

      $produtos = ProdutoDAO::getAll();
//        require_once "view/cadastroE.php";

      $clientes = ClienteDAO::getAll();

        require_once "view/cadastroE.php";

    }

  



}

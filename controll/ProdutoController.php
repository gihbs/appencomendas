<?php

require_once 'model/Conexao.php';
require_once 'model/Produto.php';
require_once 'model/ProdutoDAO.php';

$con = Conexao::connect(); 

class ProdutoController {
    
    public static function inserir(){
        
        $nome = $_POST['nome'];
         if(!isset($_SESSION))
    { 
        session_start(); 
    }
        $idusuario = $_SESSION['id']; 

        $p = new Produto($nome, $idusuario);
        
        ProdutoDAO::insert($p);

        require_once "index.php";
    }

    public static function listar(){

        require_once 'model/ProdutoDAO.php';

        $produtos = ProdutoDAO::getAll();

        require_once "view/cadastroP.php";

    }
    

}

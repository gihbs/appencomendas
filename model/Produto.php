<?php

class Produto {
    public $idproduto;
    public $nome;
    public $idusuario; 

    function __construct($nome, $idusuario, $idproduto = 0) {
        $this->nome = $nome;                
        $this->idusuario = $idusuario;
        $this->idproduto = $idproduto;

    }

    function getIdproduto() {
        return $this->idproduto;
    }

    function getIdusuario() {
        return $this->idusuario;
    }

    function getNome() {
        return $this->nome;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }
     
         
}
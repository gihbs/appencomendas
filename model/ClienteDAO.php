<?php 

require_once 'Conexao.php';
require_once 'Cliente.php';

class ClienteDAO{

    public static function insert(Cliente $cliente){

    $con = Conexao::connect();
        $stmt = $con->prepare("insert into cliente(nome, idusuario, telefone, idcliente) values(?,?,?,?)");
        $stmt->bind_param("sisi", $nome, $idusuario, $telefone, $idcliente);
        $idusuario = $cliente->getIdusuario();
        $nome = $cliente->getNome();
        $telefone = $cliente->getTelefone();
        $idcliente = $cliente->getIdCliente();
        
        $result = $stmt->execute();
        //Conexao::close();
        return $result;
    }

    public static function update(){}

    public static function getAll(){

                $con = Conexao::connect();
                $stmt = $con->prepare("select nome, idusuario, telefone, idcliente from cliente where idusuario =".$_SESSION["id"]);
                
                if($stmt->execute() == TRUE){
                    $stmt->bind_result($nome, $idusuario,$telefone, $idcliente);
                    $clientes = array();
                    
                    while($stmt->fetch()) {
                        $c = new Cliente($nome, $idusuario, $telefone, $idcliente);
                        array_push($clientes, $c);                
                    }
                    //Conexao::close();
                    return $clientes;
                }
                //Conexao::close();
                return null;
        }

    public static function getById($id){

        $con = Conexao::connect();
        $stmt = $con->prepare("select nome, telefone, idusuario from cliente where idcliente = ?");
        $stmt->bind_param("i", $idcliente);
        $idcliente = $id;
        
        if($stmt->execute() == TRUE){
            $stmt->bind_result($nome, $telefone, $idusuario);
            $stmt->fetch();
            $c = new Cliente($nome, $idusuario, $telefone, $idcliente);
            //Conexao::close();
            return $c;
        }
        
        //Conexao::close();
        return null;
    }

    public static function delete(){}
}



?>
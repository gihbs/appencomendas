<?php 

require_once 'Conexao.php';
require_once 'Produto.php';


class ProdutoDAO{

    public static function insert(Produto $produto){

        $con = Conexao::connect();
        $stmt = $con->prepare("insert into produto(nome, idusuario, idproduto) values(?,?,?)");
        $stmt->bind_param("sii", $nome, $idusuario, $idproduto);
        $idusuario = $produto->getIdusuario();
        $nome = $produto->getNome();
        $idproduto = $produto->getIdproduto();
        
        $result = $stmt->execute();
        //Conexao::close();
        return $result;
    }

    public static function update(){}

    public static function getAll(){
                 $con = Conexao::connect();
                $stmt = $con->prepare("select nome, idusuario, idproduto from produto where idusuario =".$_SESSION["id"]);
                
                if($stmt->execute() == TRUE){
                    $stmt->bind_result($nome, $idusuario, $idproduto);
                    $produtos = array();
                    
                    while($stmt->fetch()) {
                        $p = new Produto($nome, $idusuario, $idproduto);
                        array_push($produtos, $p);                
                    }
                    //Conexao::close();
                    return $produtos;
                }
                //Conexao::close();
                return null;
        }

    public static function getById($id){

        $con = Conexao::connect();
        $stmt = $con->prepare("select nome, idusuario from produto where idproduto = ?");
        $stmt->bind_param("i", $idproduto);
        $idproduto = $id;
        
        if($stmt->execute() == TRUE){
            $stmt->bind_result($nome, $idusuario);
            $stmt->fetch();
            $p = new Produto($nome, $idusuario, $idproduto);
            //Conexao::close();
            return $p;
        }
        
        //Conexao::close();
        return null;
    }

    public static function delete(){}
}



?>
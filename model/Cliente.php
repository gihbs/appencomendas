<?php 

class Cliente {
    public $idcliente;
    public $idusuaruio; 
    public $nome;
    public $telefone;

    function __construct($nome, $idusuario, $telefone, $idcliente = 0) {
        $this->nome = $nome;
        $this->telefone = $telefone;
        $this->idcliente = $idcliente;
        $this->idusuario = $idusuario;

    }
    function getIdusuario() {
        return $this->idusuario;
    }

    function getNome() {
        return $this->nome;
    }

    function getTelefone() {
        return $this->telefone;
    }
    function getIdCliente() {
        return $this->idcliente;
    }

    function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }
    function setNome($nome) {
        $this->nome = $nome;
    }
    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }


}
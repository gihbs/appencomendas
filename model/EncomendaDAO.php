<?php 

require_once 'Conexao.php';
require_once 'Encomenda.php';

class EncomendaDAO{

        public static function insert(Encomenda $encomenda){

        $con = Conexao::connect();
        $stmt = $con->prepare("insert into encomenda(data_entrega, horario_entrega, idcliente, idproduto, observacao, valor_encomenda, idusuario, idencomenda) values(?,?,?,?,?,?,?,?)");
        $stmt->bind_param("ssiissii", $data_entrega, $horario_entrega, $idcliente, $idproduto, $observacao, $valor_encomenda, $idusuario, $idencomenda);
        $data_entrega = $encomenda->getData_entrega();
        $horario_entrega = $encomenda->getHorario_entrega();
        $idcliente = $encomenda->getIdcliente();
        $idproduto = $encomenda->getIdproduto();
        $observacao = $encomenda->getObservacao();
        $valor_encomenda = $encomenda->getValor_encomenda();
        $idusuario = $encomenda->getIdusuario();
        $idencomenda = $encomenda->getIdencomenda();
        
        $result = $stmt->execute();
        //Conexao::close();
        return $result;
        }

        public static function update(Encomenda $encomenda, $id){
            $con = Conexao::connect();
            $stmt = $con->prepare("update encomenda set data_entrega = ?, horario_entrega = ?, idcliente = ?,  idproduto = ?, valor_encomenda = ?, observacao = ?, idusuario=? where idencomenda = ?");
            $stmt->bind_param("ssiissii", $data_entrega, $horario_entrega, $idcliente, $idproduto, $valor_encomenda, $observacao, $idusuario, $idencomenda);
            $data_entrega = $encomenda->getData_entrega();
            $horario_entrega = $encomenda->getHorario_entrega();
            $idcliente = $encomenda->getIdcliente();
            $idproduto = $encomenda->getIdproduto();
            $observacao = $encomenda->getObservacao();
            $valor_encomenda = $encomenda->getValor_encomenda();
            $idusuario = $encomenda->getIdusuario();
            $idencomenda = $id;
            
            $result = $stmt->execute();
            //Conexao::close();
            return $result;
        }

        public static function updateStatus($id, $novo_status) {
            $con = Conexao::connect();
            $stmt = $con->prepare("update encomenda set status_encomenda = ? where idencomenda = ?");
            $stmt->bind_param("ii", $status_encomenda, $idencomenda);
            
            $status_encomenda = $novo_status;
            $idencomenda = $id;
            
            $result = $stmt->execute();
            //Conexao::close();
       
                return $result;
 
        }

        public static function getAll(){

                $con = Conexao::connect();
                $stmt = $con->prepare("select idproduto, idcliente, data_entrega, horario_entrega, valor_encomenda, observacao, status_encomenda, idencomenda from encomenda order by data_entrega asc");
                
                if($stmt->execute() == TRUE){
                    $stmt->bind_result($idproduto, $idcliente, $data_entrega, $horario_entrega, $valor_encomenda, $observacao, $status_encomenda, $idencomenda);
                    $encomendas = array();
                    
                    while($stmt->fetch()) {
                        $e = new Encomenda($idproduto, $idcliente, $data_entrega, $horario_entrega, $valor_encomenda, $observacao, $status_encomenda, $idencomenda);
                        array_push($encomendas, $e);                
                    }
                   // Conexao::close();
                    return $encomendas;
                }
                //Conexao::close();
                return null;
        }

        public static function getById($id){

            $con = Conexao::connect();
            $stmt = $con->prepare("select idproduto, idcliente, data_entrega, horario_entrega, valor_encomenda, observacao, idusuario, status_encomenda from encomenda where idencomenda = ?");
            $stmt->bind_param("i", $idencomenda);
            $idencomenda = $id;
            
            if($stmt->execute() == TRUE){
                $stmt->bind_result($idproduto, $idcliente, $data_entrega, $horario_entrega, $valor_encomenda, $observacao,$idusuario, $status_encomenda);
                $stmt->fetch();
                $e = new Encomenda($idproduto, $idcliente, $data_entrega, $horario_entrega, $valor_encomenda, $observacao, $idusuario, $status_encomenda, $idencomenda);
                //Conexao::close();
                return $e;
            }
            
            //Conexao::close();
            return null;
        }

        

        public static function getByIdUsuario(){

            $con = Conexao::connect();
            $stmt = $con->prepare("select idproduto, encomenda.idcliente, data_entrega, horario_entrega, 
            valor_encomenda, observacao, usuario.idusuario, status_encomenda, encomenda.idencomenda from encomenda
            inner join cliente on encomenda.idcliente = cliente.idcliente
            inner join usuario on cliente.idusuario = usuario.idusuario
            where usuario.idusuario =".$_SESSION["id"]);
            
            if($stmt->execute() == TRUE){
                $stmt->bind_result($idproduto, $idcliente, $data_entrega, $horario_entrega, $valor_encomenda, $observacao, $idusuario, $status_encomenda, $idencomenda);

                $encomendas = array();
                    
                while($stmt->fetch()) {
                        $e = new Encomenda($idproduto, $idcliente, $data_entrega, $horario_entrega, $valor_encomenda, $observacao, $idusuario, $status_encomenda, $idencomenda);
                        array_push($encomendas, $e);                
                }
                //Conexao::close();
                //print_r($e); exit();
                return $encomendas;

                    
            }
            
            //Conexao::close();
            return null;
        }

        public static function delete($id){
            $con = Conexao::connect();
            $stmt = $con->prepare("delete from encomenda where idencomenda = ?");
            $stmt->bind_param("i", $idencomenda);
            $idencomenda = $id;
            
            $result = $stmt->execute();
            //Conexao::close();
            return $result;
        }
}



?>
<?php

class Usuario {
    public $idusuario;
    public $nome;
    public $user;
    public $senha;

    function __construct($nome, $user, $senha,$idusuario = 0) {
        $this->nome = $nome;
        $this->user = $user;
        $this->senha = $senha;
        $this->idusuario = $idusuario;

    }
    function getIdusuario() {
        return $this->idusuario;
    }

    function getNome() {
        return $this->nome;
    }

    function getUser() {
        return $this->user;
    }
    function getSenha() {
        return $this->senha;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }
     function setUser($user) {
        $this->user = $user;
    }
     function setSenha($senha) {
        $this->senha = $senha;
    }
}
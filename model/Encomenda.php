<?php

class Encomenda {
    public $idencomenda;
    public $idproduto;
    public $idcliente;
    public $data_entrega;
    public $horario_entrega;
    public $valor_encomenda;
    public $observacao;
    public $status_encomenda;
    public $idusuario;

    function __construct($idproduto, $idcliente, $data_entrega, $horario_entrega, $valor_encomenda, $observacao,$idusuario, $status_encomenda = 0, $idencomenda = 0) {
        $this->idproduto = $idproduto;
        $this->idcliente = $idcliente;
        $this->data_entrega = $data_entrega;
        $this->horario_entrega = $horario_entrega;
        $this->valor_encomenda = $valor_encomenda;
        $this->observacao = $observacao;
        $this->idencomenda = $idencomenda;
        $this->status_encomenda = $status_encomenda;
        $this->idusuario = $idusuario;

    }

    function getIdencomenda() {
        return $this->idencomenda;
    }
    function getIdusuario() {
        return $this->idusuario;
    }
    function getIdproduto() {
        return $this->idproduto;
    }

    function getIdcliente() {
        return $this->idcliente;
    }

    function getData_entrega() {
        return $this->data_entrega;
    }

    function getHorario_entrega() {
        return $this->horario_entrega;
    }

    function getValor_encomenda() {
        return $this->valor_encomenda;
    }

    function getObservacao() {
        return $this->observacao;
    }
    function getStatus_encomenda() {
        return $this->status_encomenda;
    }


    function setIdproduto($idproduto) {
        $this->idproduto = $idproduto;
    }
    function setIdusuario($idusuario) {
        $this->idusuario = $idusuario;
    }
    function setIdcliente($idcliente) {
        $this->idcliente = $idcliente;
    }

    function setData_entrega($data_entrega) {
        $this->data_entrega = $data_entrega;
    }

    function setHorario_entrega($horario_entrega) {
        $this->horario_entrega = $horario_entrega;
    }

    function setValor_encomenda($valor_encomenda) {
        $this->valor_encomenda = $valor_encomenda;
    }

    function setObservacao($observacao) {
        $this->observacao = $observacao;
    }    
    function setStatus_encomenda($status_encomenda) {
        $this->status_encomenda = $status_encomenda;
    }





}